#ifndef __IMAGE_SEPIA_H__
#define __IMAGE_SEPIA_H__

#ifdef REQUIRE_SEPIA_INPLACE_FUNCTION
// the function "sepia_inplace" is only defined when exactly one of SEPIA_FAST_IMPL or SEPIA_SLOW_IMPL
// is defined.
// this is because its required exactly one of the implementation.
#   if !defined(SEPIA_FAST_IMPL) && !defined(SEPIA_SLOW_IMPL)
#       error "One of macro SEPIA_FAST_IMPL and SEPIA_SLOW_IMPL must be defined."
#   endif
#   if defined(SEPIA_FAST_IMPL) && defined(SEPIA_SLOW_IMPL)
#       error "ONLY one of macro SEPIA_FAST_IMPL and SEPIA_SLOW_IMPL must be defined."
#   endif
#endif

#include <stdint.h>
#include "pixel.h"
#include "image.h"

extern const float SEPIA_COEFFICIENT[3][3];

unsigned char sat(uint64_t x);
void sepia_one(struct pixel* const pixel);

#ifdef SEPIA_FAST_IMPL
void fast_sepia_inplace(struct image* img);
#   ifndef SEPIA_SLOW_IMPL
    static inline void sepia_inplace(struct image* img) {
        fast_sepia_inplace(img);
    }
#   endif
#endif

#ifdef SEPIA_SLOW_IMPL
void slow_sepia_inplace(struct image* img);
#   ifndef SEPIA_FAST_IMPL
    static inline void sepia_inplace(struct image* img) {
        slow_sepia_inplace(img);
    }
#   endif
#endif


#endif // __IMAGE_SEPIA_H__
