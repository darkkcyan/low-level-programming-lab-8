#include <stdint.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h> 
#include "pixel.h"
#include "image.h"
#include "image-sepia.h"
#include "bmp.h"

const float pi = 4 * atan(1);
const float twice_pi = 2 * pi;

float vertical_progress(uint32_t width, uint32_t height, uint32_t x, uint32_t y) {
    return (float) x / width;
}

float horizontal_progress(uint32_t width, uint32_t height, uint32_t x, uint32_t y) {
    return (float) y / height;
}

float circular_progress(uint32_t width, uint32_t height, uint32_t x, uint32_t y) {
    long dx = (long)x - width / 2;
    long dy = (long)y - height / 2;
    
    float ang = atan2(dx * 1.0 / width * height, dy);  // i know that the order is y then x, but I like this order
    return (ang + pi) / twice_pi;
}

int main(int argc, char** argv) {
    if (argc < 5) {
        printf("Usage: %s width height func filename\n", argv[0]);
        printf("func can be either \"vertical\", \"horizontal\" or \"circular\".\n");
        return 0;
    }
    
    uint32_t width;
    uint32_t height;
    char* func = argv[3];
    char* filename = argv[4];
    
    if (sscanf(argv[1], "%u", &width) != 1) {
        fprintf(stderr, "Error: width must be an unsigned 32-bit integer\n");
        return 1;
    }
    
    if (sscanf(argv[2], "%u", &height) != 1) {
        fprintf(stderr, "Error: height must be an unsigned 32-bit integer\n");
        return 1;
    }
    
    float (*progress_fn) (uint32_t, uint32_t, uint32_t, uint32_t);
    if (strcmp(func, "horizontal") == 0) {
        progress_fn = horizontal_progress;
    } else if (strcmp(func, "vertical") == 0) {
        progress_fn = vertical_progress;
    } else if (strcmp(func, "circular") == 0) {
        progress_fn = circular_progress;
    } else {
        printf("Error: func must be either \"horizontal\", \"vertical\" or \"circular\".\n");
        return 1;
    }
    
    FILE* f = fopen(filename, "wb");
    if (!f) {
        fprintf(stderr, "Cannot open file %s for writing.\n", f);
        return 1;
    }
    
    struct image img;
    reset_image(&img, width, height);
    
    uint32_t x, y;
    for (x = 0; x < width; ++x) {
        for (y = 0; y < height; ++y) {
            float pr = progress_fn(width, height, x, y) * 6;
            struct pixel* px = pixel_of(img, x, y);
            px->r = px->g = px->b = 0;
            if (pr < 1) { 
                px->r = 255;
                px->g = sat(255 *  pr);
            } else if (pr < 2.) {
                px->r = sat(255 * (2 - pr));
                px->g = 255;
            } else if (pr < 3) {
                px->g = 255;
                px->b = sat(255 * (pr - 2));
            } else if (pr < 4) {
                px->b = 255;
                px->g = sat(255 * (4 - pr));
            } else if (pr < 5) {
                px->b = 255;
                px->r = sat(255 * (pr - 4));
            } else {
                px->r = 255;
                px->b = sat(255 * (6 - pr));
            }
        }
    }
    
    int status = image_to_bmp(f, img);
    if (status != BMP_WRITE_OK) {
        fprintf(stderr, "Error while writing image to file.\n");
        return 1;
    }
    
    free(img.data);
    fclose(f);
    
    return 0;
}
